/**
 * @file    tree_mesh_builder.cpp
 *
 * @author  FULL NAME <xlogin00@stud.fit.vutbr.cz>
 *
 * @brief   Parallel Marching Cubes implementation using OpenMP tasks + octree early elimination
 *
 * @date    DATE
 **/

#include <iostream>
#include <math.h>
#include <limits>
#include <omp.h>

#include "tree_mesh_builder.h"

const float sqrt3 = sqrt(3);

TreeMeshBuilder::TreeMeshBuilder(unsigned gridEdgeSize)
    : BaseMeshBuilder(gridEdgeSize, "Octree")
{

}

unsigned TreeMeshBuilder::marchCubes(const ParametricScalarField &field)
{
    // Suggested approach to tackle this problem is to add new method to
    // this class. This method will call itself to process the children.
    // It is also strongly suggested to first implement Octree as sequential
    // code and only when that works add OpenMP tasks to achieve parallelism.


	Vec3_t<float> minIndex(0, 0, 0);
	Vec3_t<float> maxIndex(mGridSize, mGridSize, mGridSize);

	std::cerr << minIndex.x << " " << minIndex.y << " " <<minIndex.z << " " << maxIndex.x << " " << maxIndex.y << " " << maxIndex.z << "\n";

	return marchCubesOctree(field, minIndex, maxIndex);

	/*
	// 1. Compute total number of cubes in the grid.
    size_t totalCubesCount = mGridSize*mGridSize*mGridSize;

    unsigned totalTriangles = 0;

    // 2. Loop over each coordinate in the 3D grid.
    for(size_t i = 0; i < totalCubesCount; ++i)
    {
        // 3. Compute 3D position in the grid.
        Vec3_t<float> cubeOffset( i % mGridSize,
                                 (i / mGridSize) % mGridSize,
                                  i / (mGridSize*mGridSize));

        // 4. Evaluate "Marching Cube" at given position in the grid and
        //    store the number of triangles generated.
        totalTriangles += buildCube(cubeOffset, field);
    }

    // 5. Return total number of triangles generated.
    return totalTriangles;*/
}

float TreeMeshBuilder::evaluateFieldAt(const Vec3_t<float> &pos, const ParametricScalarField &field, float threshold)
{
	// NOTE: This method is called from "buildCube(...)"!

    // 1. Store pointer to and number of 3D points in the field
    //    (to avoid "data()" and "size()" call in the loop).
	//threshold *= threshold;
    const Vec3_t<float> *pPoints = field.getPoints().data();
    const unsigned count = unsigned(field.getPoints().size());

    float value = std::numeric_limits<float>::max();

    // 2. Find minimum square distance from points "pos" to any point in the
    //    field.
    for(unsigned i = 0; i < count; ++i)
    {
        float distanceSquared  = (pos.x - pPoints[i].x) * (pos.x - pPoints[i].x);
        distanceSquared       += (pos.y - pPoints[i].y) * (pos.y - pPoints[i].y);
        distanceSquared       += (pos.z - pPoints[i].z) * (pos.z - pPoints[i].z);

        // Comparing squares instead of real distance to avoid unnecessary
        // "sqrt"s in the loop.
        value = std::min(value, distanceSquared);
		//if (sqrt(distanceSquared) < threshold)
		//	return 0;
    }

    // 3. Finally take square root of the minimal square distance to get the real distance
	if (sqrt(value) < threshold)
	{
		std::cerr << "\n" << sqrt(value) << " " << threshold << " " << "true ";
		return -1;
	}
    return sqrt(value);
	return 10000;
}

void TreeMeshBuilder::emitTriangle(const BaseMeshBuilder::Triangle_t &triangle)
{
	// NOTE: This method is called from "buildCube(...)"!

    //int myid = omp_get_thread_num();
    mTrianglesRes.push_back(triangle);
}

unsigned TreeMeshBuilder::marchCubesOctree(const ParametricScalarField &field, const Vec3_t<float>& minIndex, const Vec3_t<float>& maxIndex)
{
	unsigned octreeSize = maxIndex.x - minIndex.x;
	float octreeSizeHalf = octreeSize / (float) 2;

	Vec3_t<float> center((octreeSizeHalf + minIndex.x) * mGridResolution,
						(octreeSizeHalf + minIndex.y) * mGridResolution,
						(octreeSizeHalf + minIndex.z) * mGridResolution);
	float a = mIsoLevel + sqrt3 * octreeSizeHalf * mGridResolution;
	float value_in_center = evaluateFieldAt(center, field, a);
	if(value_in_center >= a)
		return 0;

	if(octreeSize == 1)
	{
		return buildCube(minIndex, field);
	}

	unsigned totalTriangles = 0;
	for(unsigned i = 0; i < 8; ++i)
    {
		Vec3_t<float> min(minIndex.x + (i % 2) * octreeSizeHalf,
						  minIndex.y + ((i / 2) % 2) * octreeSizeHalf,
						  minIndex.z + (i / 4) * octreeSizeHalf);

		Vec3_t<float> max(minIndex.x + (i % 2) * octreeSizeHalf + octreeSizeHalf,
						  minIndex.y + ((i / 2) % 2) * octreeSizeHalf + octreeSizeHalf,
						  minIndex.z + (i / 4) * octreeSizeHalf + octreeSizeHalf);
        totalTriangles += marchCubesOctree(field, min, max);
    }
	return totalTriangles;
}
