/**
 * @file    tree_mesh_builder.cpp
 *
 * @author  FULL NAME <xlogin00@stud.fit.vutbr.cz>
 *
 * @brief   Parallel Marching Cubes implementation using OpenMP tasks + octree early elimination
 *
 * @date    DATE
 **/

#include <iostream>
#include <math.h>
#include <limits>
#include <omp.h>

#include "tree_mesh_builder.h"

const float sqrt3 = sqrt(3);

TreeMeshBuilder::TreeMeshBuilder(unsigned gridEdgeSize)
    : BaseMeshBuilder(gridEdgeSize, "Octree")
{

}

unsigned TreeMeshBuilder::marchCubes(const ParametricScalarField &field)
{
    // Suggested approach to tackle this problem is to add new method to
    // this class. This method will call itself to process the children.
    // It is also strongly suggested to first implement Octree as sequential
    // code and only when that works add OpenMP tasks to achieve parallelism.


	Vec3_t<float> minIndex(0, 0, 0);
	Vec3_t<float> maxIndex(mGridSize, mGridSize, mGridSize);

	#pragma omp parallel
	{
		#pragma omp single
		{
			int nt = omp_get_num_threads();
            mTriangles.resize(nt);
			marchCubesOctree(field, minIndex, maxIndex);
		}
	}

	for(auto& triangles : mTriangles)
	{
		mTrianglesRes.insert(mTrianglesRes.end(), triangles.begin(), triangles.end());
	}

	return mTrianglesRes.size();
}

float TreeMeshBuilder::evaluateFieldAt(const Vec3_t<float> &pos, const ParametricScalarField &field)
{
	// NOTE: This method is called from "buildCube(...)"!

    // 1. Store pointer to and number of 3D points in the field
    //    (to avoid "data()" and "size()" call in the loop).
    const Vec3_t<float> *pPoints = field.getPoints().data();
    const unsigned count = unsigned(field.getPoints().size());

    float value = std::numeric_limits<float>::max();

    // 2. Find minimum square distance from points "pos" to any point in the
    //    field.
    for(unsigned i = 0; i < count; ++i)
    {
        float distanceSquared  = (pos.x - pPoints[i].x) * (pos.x - pPoints[i].x);
        distanceSquared       += (pos.y - pPoints[i].y) * (pos.y - pPoints[i].y);
        distanceSquared       += (pos.z - pPoints[i].z) * (pos.z - pPoints[i].z);

        // Comparing squares instead of real distance to avoid unnecessary
        // "sqrt"s in the loop.
        value = std::min(value, distanceSquared);
    }

    // 3. Finally take square root of the minimal square distance to get the real distance
    return sqrt(value);
}

void TreeMeshBuilder::emitTriangle(const BaseMeshBuilder::Triangle_t &triangle)
{
	// NOTE: This method is called from "buildCube(...)"!

	int myid = omp_get_thread_num();
    mTriangles[myid].push_back(triangle);
}

void TreeMeshBuilder::marchCubesOctree(const ParametricScalarField &field, const Vec3_t<float>& minIndex, const Vec3_t<float>& maxIndex)
{
	unsigned octreeSize = maxIndex.x - minIndex.x;
	float octreeSizeHalf = octreeSize / (float) 2;

	Vec3_t<float> center((octreeSizeHalf + minIndex.x) * mGridResolution,
						(octreeSizeHalf + minIndex.y) * mGridResolution,
						(octreeSizeHalf + minIndex.z) * mGridResolution);
	float value_in_center = evaluateFieldAt(center, field);
	if(value_in_center > mIsoLevel + sqrt3 * octreeSizeHalf * mGridResolution)
		return;

	if(octreeSize == 1)
	{
		buildCube(minIndex, field);
		return;
	}

	for(unsigned i = 0; i < 8; ++i)
    {
		Vec3_t<float> min(minIndex.x + (i % 2) * octreeSizeHalf,
						  minIndex.y + ((i / 2) % 2) * octreeSizeHalf,
						  minIndex.z + (i / 4) * octreeSizeHalf);

		Vec3_t<float> max(minIndex.x + (i % 2) * octreeSizeHalf + octreeSizeHalf,
						  minIndex.y + ((i / 2) % 2) * octreeSizeHalf + octreeSizeHalf,
						  minIndex.z + (i / 4) * octreeSizeHalf + octreeSizeHalf);

		#pragma omp task final(octreeSize < 8)
        marchCubesOctree(field, min, max);
    }
	#pragma omp taskwait
}
